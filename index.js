// Soal 1 ==========================================================================
console.log("Soal No.1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var n_data = daftarHewan.length;

daftarHewan_urut = daftarHewan.sort();

// for (var i = 0; i < n_data; i++) {
//     console.log(daftarHewan_urut[i]);
// }


daftarHewan_urut.forEach(function(item) {
    console.log(item);
})


console.log("");
console.log("");


// Soal 2 ==========================================================================
console.log("Soal No.2");

function introduce(data) {
    nama = data.name;
    umur = data.age;
    alamat = data.address;
    hobi = data.hobby;
    return "Nama saya " + nama + " , umur saya " + umur + " tahun, alamat saya di " + alamat + " , dan saya punya hobby yaitu " + hobi;

}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan);

console.log("");
console.log("");



// Soal 3 ==========================================================================
console.log("Soal No.3");

function hitung_huruf_vokal(input) {
    let banyakHurufVocal = 0;
    let hurufInput = input.split("");
    let huruf_vocal = ["a", "i", "u", "e", "o", "A", "I", "U", "E", "O"];

    hurufInput.forEach(function(item) {
        if (huruf_vocal.indexOf(item) != -1) {
            banyakHurufVocal++;
        }
    })

    return banyakHurufVocal;

}


var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

// Soal 4 ==========================================================================
console.log("Soal No.4");

function hitung(input) {
    hasil = 2 * (input - 1);
    return hasil;
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8